package drupalsimulation

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class DrupalSimulation extends Simulation {

  // Set the Url, login and password
  val WebsiteUrl = "https://DRUPAL-WEBSITE-URL"
  val DrupalUserLogin = "DRUPAL-LOGIN"
  val DrupalUserPassword = "DRUPAL-PASSWORD"

  // Init The http protocol.
  val httpProtocol = http
    .baseUrl(WebsiteUrl)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  // Define the scenatio.
  val scn = scenario("Admin")
    .exec(
      http("Home_page")
        // Go to homepage.
        .get("/")
        // Test the Http status code and check for content.
        .check(
          status.is(200),
          substring("Welcome to Gatling")
        )
    )
    // Simulate a delay of 3 seconds.
    .pause(3)
    .exec(
      http("goto_login")
        // Go to login page.
        .get("/user/login")
        .check(
          // Check the Http status code.
          status.is(200),
          // Get the values of form_build_id, form_id et op.
          // Store the values in variable in order to use them later
          // to submit the login form.
          substring("Log in"),
          css("input[name=form_build_id]", "value").saveAs("form_build_id"),
          css("input[name=form_id]", "value").saveAs("form_id"),
          css("input[name=op]", "value").saveAs("op"),
        )
    )
    // Simulate a delay of 5 seconds.
    .pause(5)
    .exec(
      http("Login")
        // Perform a Http POST request using the variables previsouly stored.
        .post("/user/login")
        .formParamSeq(
          Seq(
            ("name", DrupalUserLogin),
            ("pass", DrupalUserPassword),
            ("form_build_id", "${form_build_id}"),
            ("form_id", "${form_id}"),
            ("op", "${op}")
          )
        )
        // Check the response content.
        .check(substring("Member for"))
    )
    .pause(3)
    .exec(
      http("404")
        // Call an invalid url
        .get("/not-found")
        // Check the Http response status code.
        .check(
          status.is(404),
        )
    )

  // Launch the simulation.
  setUp(
    scn
    .inject(
      atOnceUsers(10) // Simulate 10 concurent users.
    )
    .protocols(httpProtocol)
  )
}

