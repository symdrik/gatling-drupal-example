# What is this?

This is an example script that performs Load testing on a Drupal site using Gatling.

# Usage

* Clone this repo.
* Edit the file ```simulation/DrupalSimulation.scala``` and set the variables ```WebsiteUrl```, ```DrupalUserLogin``` and  ```DrupalUserPassword```.
* Run the following command
```
docker run -it --rm \
-v $(pwd)/simulation:/opt/gatling/simulation \
-v $(pwd)/resultat:/opt/gatling/resultat \
denvazh/gatling -sf simulation -rd "Drupal simulation" -rf resultat
```
* Check the report inside ```resultat``` folder.